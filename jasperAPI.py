# TODO Will handle Jasper CC API calls
from email import base64mime
from email.mime import base
import requests
import json
import pprint


class API:
    api_key: str = "9c5b4f50-2bdf-4d5d-b025-9acfeb818791"
    api_username: str = "RaulBacaTelus"

    def set_api_key(self, key: str):
        API.api_key = key

    def set_api_username(self, username: str):
        API.api_username = username

    def api_get(self, url: str):
        response = requests.get(url, auth=(API.api_username, API.api_key))

        if response.ok:
            jData = json.loads(response.content)
            pp = pprint.PrettyPrinter(indent=4)
            pp.pprint(jData)
        else:
            # If response code is not ok (200), print the resulting http error code with description
            print("Failure")
            response.raise_for_status()

    def get_session_details(self, ICCID: str):
        url: str = (
            "https://restapi7.jasper.com/rws/api/v1/devices/"
            + ICCID
            + "/sessionInfo"
        )

        API.api_get(self, url)

    def get_device_details(self, ICCID: str):
        url = "https://restapi7.jasper.com/rws/api/v1/devices/" + ICCID
        API.api_get(self, url)
