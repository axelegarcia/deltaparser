# deltaParser
deltaParser allows you to input a .log or .txt file, which is composed of event logs copied and pasted from the admin [site](https://admin.positioninguniversal.com/), and in return receive a parsed .csv and graphs plotting the parsed data. 
## Use
---
***ONLY WORKS WITH LOGS THAT WERE CAPTURED WHILE DEVICE WAS IN DATABASE; OTHER LOGS WILL RESULT IN GARBAGE OUTPUT***

In order to run this program: 

1. Open a terminal or command prompt and cd to the path in which the program is located.
2. Enter `python3 deltaParser.py` or `python deltaParser.py` into the terminal or command prompt.
3. When prompted, enter the number corresponding to the device model you are trying to parse logs for.
4. When prompted, enter the number corresponding to the file you want to parse (file must be under `Logs/`).
5. Then, enter the desired name of the output file that will be created. This name will also be used as a prefix for an xlsx file that will contain a created pivot table. The created files will be under the `Results/` folder.

The output .csv resembles the following:

| Server Date | Server Time  | IMEI            | Delta(s) | Action | Latitude   | Longitude    | # Satellites | HAC  | RSSI | Speed | Main Voltage | Backup Voltage |
| ----------- | ------------ | --------------- | -------- | ------ | ---------- | ------------ | ------------ | ---- | ---- | ----- | ------------ | -------------- |
| 04/11       | 19:23:04.216 | 359206104959770 | 5.716    | MOVE   | 32.8342542 | -117.1644588 | 10           | 21.0 | 59   | 0     | 14.29        | 4.1            |
| 04/11       | 19:24:02.733 | 359206104959770 | 4.233    | MOVE   | 32.8342742 | -117.1644429 | 9            | 19.0 | 59   | 0     | 14.29        | 4.1            |
| 04/11       | 19:25:02.872 | 359206104959770 | 4.372    | MOVE   | 32.8342729 | -117.1644213 | 9            | 20.0 | 57   | 0     | 14.29        | 4.1            |

### Graphs
---
This tool now creates graphs automatically. The first graph provides the deltas binned into 20 second bins, starting from 0s all the way to 580s+. The second graph provides a view of the deltas over time, where the y-axis is the delta in seconds and the x-axis is the time the event occured.

**Example:**
![Example Output](./Example/output.png)
