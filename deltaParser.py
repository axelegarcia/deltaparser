from os import listdir
import re
import string
import pandas as pd
import numpy as np
import matplotlib.pyplot as plt


# Globals
# List of dictionaries for possible feature in which data is processed
# to output stats
lis = []
iter = -1
last_iter = -10


def ask_user_input() -> tuple[str, str, str]:
    """Function handles user input.

    Returns:
        tuple[str, str, str]: Return tuple with device model, input file,
                              and output file input by user
    """
    while True:
        device_list = ["FJ2500", "TM95"]
        # Enum choices for user
        for item in enumerate(device_list, 1):
            print("\t" + str(item[0]) + ". " + item[1])
        # Ask user to input desired device index
        try:
            device_model = int(
                input("Please enter the device model (1 or 2): ")
            )
        # If non int value entered, print error and continue for retry.
        except ValueError:
            print("\n***Please enter number index of device model.***\n")
            continue
        # Grab from list the device model by user's inputted index
        try:
            # Since list is being enumarated starting at 1, we have to account
            # for this when user inputs their choice. We do this by subtracting 1 from their
            # input as indexing in python starts at 0.
            index = int(device_model - 1)
            if index < 0:
                raise IndexError()
            device_model = device_list[index]
        # If int entered out of range, print error and continue for retry.
        except IndexError:
            print("\n***Number chosen not in range.***\n")
            continue
        # If this point is reached, no errors therefore loop breaks
        break
    while True:
        l = listdir("Logs/")
        print("Available log files:")
        for item in enumerate(l, 1):
            print("\t" + str(item[0]) + ". " + item[1])
        try:
            inpF = int(
                input(
                    "Please enter the number of the logfile you want to use (must be within Logs folder): "
                )
            )
        except ValueError:
            print("\n***Please enter number of file.***\n")
            continue
        try:
            index = int(inpF - 1)
            if index < 0:
                raise IndexError()
            inpF = "Logs/" + l[index]
        except IndexError:
            print("\n***Number chosen not in range.***\n")
            continue
        # If this point is reached, no errors therefore loop breaks
        break
    while True:
        outF = input(
            "Please enter the name of the csv file you'd like to write to (no / or \ allowed): "
        )
        if outF != "" and all(x not in outF for x in ["\\", "/"]):
            break
        else:
            print(
                "\n***Invalid input. Please ensure path or file name is correct.***\n"
            )
    if "Results/" not in outF:
        outF = "Results/" + outF
    if ".csv" not in outF:
        outF = outF + ".csv"
    return (device_model, inpF, outF)


def parse_2500_log(file: str, output_file: str):
    """Function parses logs for FJ2500 devices

    Args:
        file (str): Holds path for input file
        output_file (str): Holds path for output file
    """
    global iter, last_iter
    list = []
    temp_string = ""
    last_first_four_col = ""
    try:
        with open(str(output_file), "w+") as results:
            results.write(
                "Server Date,Server Time,IMEI,Delta(s),Action,Latitude,Longitude,# Satellites,HAC,RSSI,Speed,Main Voltage,Backup Voltage\n"
            )
            for x in file:
                if match := re.search(
                    "(\d+/\d+) (\d+:\d+:\d+\.\d+) (\d{15}) \d{15} [A-Za-z0-9&-:\s]*\s* delta: (\d{1,20})ms",
                    x,
                ):
                    temp_string = ""
                    temp_string += (
                        match.group(1)
                        + ","
                        + match.group(2)
                        + ","
                        + match.group(3)
                        + ","
                        + str(int(match.group(4)) / 1000)
                        + ","
                    )
                    last_first_four_col = (
                        match.group(1)
                        + ","
                        + match.group(2)
                        + ","
                        + match.group(3)
                        + ","
                        + str(int(match.group(4)) / 1000)
                        + ","
                    )
                    iter += 1
                elif match := re.search(
                    "(\d{0,9}:\d{0,9}:\d{0,9} )([A-Z_ ]*\s*)(\(\d+\)\s*)([-+]?\d+\.\d{7}\s*)([-+]?\d+\.\d{7}\s*)(s(\d+))*\s*((hac(\d+\.\d+))*\s*r-(\d+)\s*)*(\d+)kph\s*(\d+\s*(\d+m\s+)*(\d+\.\d+)V\s(\d+\.\d+)BV)*",
                    x,
                ):
                    if "MINOR" in match.group(2):
                        continue
                    elif last_iter == iter:
                        if match.group(4) == 00.0000000:
                            continue
                        elif match.group(10) == None:
                            temp_string += (
                                last_first_four_col
                                + match.group(2).strip()
                                + ","
                                + match.group(4)
                                + ","
                                + match.group(5)
                                + ","
                                + str(match.group(7))
                                + ","
                                + "None"
                                + ","
                                + str(match.group(11))
                                + ","
                                + str(match.group(12))
                                + ","
                                + str(match.group(15))
                                + ","
                                + str(match.group(16))
                                + "\n"
                            )
                        else:
                            temp_string += (
                                last_first_four_col
                                + match.group(2).strip()
                                + ","
                                + match.group(4)
                                + ","
                                + match.group(5)
                                + ","
                                + str(match.group(7))
                                + ","
                                + str(match.group(10))
                                + ","
                                + str(match.group(11))
                                + ","
                                + str(match.group(12))
                                + ","
                                + str(match.group(15))
                                + ","
                                + str(match.group(16))
                                + "\n"
                            )
                    else:
                        if match.group(4) == 00.0000000:
                            continue
                        elif match.group(10) == None:
                            temp_string += (
                                match.group(2).strip()
                                + ","
                                + match.group(4)
                                + ","
                                + match.group(5)
                                + ","
                                + str(match.group(7))
                                + ","
                                + "None"
                                + ","
                                + str(match.group(11))
                                + ","
                                + str(match.group(12))
                                + ","
                                + str(match.group(15))
                                + ","
                                + str(match.group(16))
                                + "\n"
                            )
                        else:
                            temp_string += (
                                match.group(2).strip()
                                + ","
                                + match.group(4)
                                + ","
                                + match.group(5)
                                + ","
                                + str(match.group(7))
                                + ","
                                + str(match.group(10))
                                + ","
                                + str(match.group(11))
                                + ","
                                + str(match.group(12))
                                + ","
                                + str(match.group(15))
                                + ","
                                + str(match.group(16))
                                + "\n"
                            )
                    last_iter = iter
                    list.append(temp_string)

                elif match := re.search(
                    "(Ignore: Duplicate: date matches)",
                    x,
                ):
                    # Remove last line appended to list since it's a dupe
                    if len(list) > 0:
                        list.pop(-1)
                    else:
                        continue
            line_seen = set()
            for item in list:
                if item not in line_seen:
                    line_seen.add(item)
                    results.write(item)
                else:
                    continue
        file.close()
    except Exception as e:
        print(str(e))


def parse_502_log(file: str, output_file: str):
    """Parse logs for 502 devices

    Args:
        file (str): Holds path for input file
        output_file (str): Holds path for output file
    """
    try:
        with open(str(output_file), "w") as results:
            results.write(
                "Date,Time,IMEI,Event,Latitude,Longitude,Voltage,RC,DC,Cumulative Usage\n"
            )
            for x in file:
                if match := re.search(
                    "(\d{15})\s*(\d{2}/\d{2}) (\d{0,9}:\d{0,9}:\d{0,9} )([A-Z_\s*]*\s*)(\(\d+\)\s*)([-+]?\d+\.\d{7}\s*)([-+]?\d+\.\d{7}\s*)s\d+\s*(hac\d+.\d+)* (r-\d+)*\s*\d+kph\s*\d*\s*\d*m*\s*(\d+.\d{,5})BV\s*\d+s\s*(\d+)rc\s*(\d*)dc",
                    x,
                ):
                    results.write(
                        match.group(2)
                        + ","
                        + match.group(3)
                        + ","
                        + match.group(1)
                        + ","
                        + match.group(4)
                        + ","
                        + match.group(6)
                        + ","
                        + match.group(7)
                        + ","
                        + match.group(10)
                        + ","
                        + match.group(11)
                        + ","
                        + match.group(12)
                        + ","
                    )
                    iter += 1
                elif match := re.search(
                    "(\d{15})\s*(\d{2}/\d{2}) (\d{0,9}:\d{0,9}:\d{0,9} )([A-Z_\s*]*\s*)(\(\d+\)\s*)([-+]?\d+\.\d{7}\s*)([-+]?\d+\.\d{7}\s*)s\d+\s*(hac\d+.\d+ r-\d+)*\s*\d+kph\s*\d*\s*\d*m*\s*(\d+.\d{,5})BV\s*\d+s\s*(\d*)dc",
                    x,
                ):
                    results.write(
                        match.group(2)
                        + ","
                        + match.group(3)
                        + ","
                        + match.group(1)
                        + ","
                        + match.group(4)
                        + ","
                        + match.group(6)
                        + ","
                        + match.group(7)
                        + ","
                        + match.group(9)
                        + ","
                        + "N/A"
                        + ","
                        + match.group(10)
                        + ","
                    )
                    iter += 1
                elif match := re.search(
                    "(\d{15})\s*(\d{2}/\d{2}) (\d{0,9}:\d{0,9}:\d{0,9} )([A-Z_\s*]*\s*)(\(\d+\)\s*)([-+]?\d+\.\d{7}\s*)([-+]?\d+\.\d{7}\s*)s\d+\s*(hac\d+.\d+ r-\d+)*\s*\d+kph\s*\d*\s*\d*m*\s*(\d+.\d{,5})BV\s*\d+s\s*(\d*)rc$",
                    x,
                ):
                    results.write(
                        match.group(2)
                        + ","
                        + match.group(3)
                        + ","
                        + match.group(1)
                        + ","
                        + match.group(4)
                        + ","
                        + match.group(6)
                        + ","
                        + match.group(7)
                        + ","
                        + match.group(9)
                        + ","
                        + match.group(10)
                        + ","
                        + "N/A"
                        + ","
                    )
                    iter += 1
                elif match := re.search("cumulativeUsage = (\d+.\d+)", x):
                    if iter != last_iter:
                        results.write(match.group(1) + "\n")
                    ignore_scenario = match.group(1) + "\n"
                    last_iter = iter
                elif match := re.search("Ignore: ", x) and iter != last_iter:
                    results.write(ignore_scenario)
    except Exception as e:
        print(str(e))


def parse_TM95_log(file, output_file):
    """Parse logs for TM95 devices

    Args:
        file (str): Holds path for input file
        output_file (str): Holds path for output file
    """
    global iter, last_iter
    list = []
    last_first_four_col = ""
    temp_string = ""
    try:
        with open(str(output_file), "w+") as results:
            results.write(
                "Server Date,Server Time,IMEI,Delta(s),Action,Latitude,Longitude,# Satellites,DOP,RSSI,Speed,Main Voltage,Backup Voltage\n"
            )
            for x in file:
                if match := re.search(
                    "(\d+/\d+) (\d+:\d+:\d+\.\d+) (\d{15}) \d{15} [A-Za-z0-9:\s]*\s* delta: (\d+)ms",
                    x,
                ):
                    temp_string = ""
                    temp_string += (
                        match.group(1)
                        + ","
                        + match.group(2)
                        + ","
                        + match.group(3)
                        + ","
                        + str(int(match.group(4)) / 1000)
                        + ","
                    )
                    last_first_four_col = (
                        match.group(1)
                        + ","
                        + match.group(2)
                        + ","
                        + match.group(3)
                        + ","
                        + str(int(match.group(4)) / 1000)
                        + ","
                    )
                    iter += 1
                elif match := re.search(
                    "(\d{0,9}:\d{0,9}:\d{0,9} )([A-Z_ ]*\s*)(\(\d+\)\s*)([-+]?\d+\.\d{7}\s*)([-+]?\d+\.\d{7}\s*)(s(\d+))*\s*((dop(\d+))*\s*r-*(\d+)\s*)*(\d+)kph\s*(\d+\s*(\d+m\s+)*((\d+\.\d+)V)*\s*(\d+\.\d+)BV)*",
                    x,
                ):
                    if last_iter == iter:
                        if match.group(4) == 00.0000000:
                            continue
                        elif match.group(10) == None:
                            temp_string += (
                                last_first_four_col
                                + match.group(2).strip()
                                + ","
                                + match.group(4)
                                + ","
                                + match.group(5)
                                + ","
                                + str(match.group(7))
                                + ","
                                + "None"
                                + ","
                                + str(match.group(11))
                                + ","
                                + str(match.group(12))
                                + ","
                                + str(match.group(16))
                                + ","
                                + str(match.group(17))
                                + "\n"
                            )
                        else:
                            temp_string += (
                                last_first_four_col
                                + match.group(2).strip()
                                + ","
                                + match.group(4)
                                + ","
                                + match.group(5)
                                + ","
                                + str(match.group(7))
                                + ","
                                + str(match.group(10))
                                + ","
                                + str(match.group(11))
                                + ","
                                + str(match.group(12))
                                + ","
                                + str(match.group(16))
                                + ","
                                + str(match.group(17))
                                + "\n"
                            )
                    else:
                        if match.group(4) == 00.0000000:
                            continue
                        elif match.group(10) == None:
                            temp_string += (
                                match.group(2).strip()
                                + ","
                                + match.group(4)
                                + ","
                                + match.group(5)
                                + ","
                                + str(match.group(7))
                                + ","
                                + "None"
                                + ","
                                + str(match.group(11))
                                + ","
                                + str(match.group(12))
                                + ","
                                + str(match.group(16))
                                + ","
                                + str(match.group(17))
                                + "\n"
                            )
                        else:
                            temp_string += (
                                match.group(2).strip()
                                + ","
                                + match.group(4)
                                + ","
                                + match.group(5)
                                + ","
                                + str(match.group(7))
                                + ","
                                + str(match.group(10))
                                + ","
                                + str(match.group(11))
                                + ","
                                + str(match.group(12))
                                + ","
                                + str(match.group(16))
                                + ","
                                + str(match.group(17))
                                + "\n"
                            )
                    last_iter = iter
                    list.append(temp_string)
                elif match := re.search(
                    "(Ignore: Duplicate: date matches)",
                    x,
                ):
                    # Remove last line appended to list since it's a dupe
                    if len(list) > 0:
                        list.pop(-1)
                    else:
                        continue
            line_seen = set()
            for item in list:
                if item not in line_seen:
                    line_seen.add(item)
                    results.write(item)
                else:
                    continue
        file.close()
    # print(lis)
    except Exception as e:
        print(str(e))


def create_pivot_table(dataframe: pd.DataFrame) -> pd.DataFrame:
    """Function creates a pivot table from the parsed data.

    Args:
        dataframe (pd.DataFrame): DataFrame created from parsed log
        data.

    Returns:
        pd.DataFrame: Pivot table holding all pertinent information such as
        count of actions and mean, min, and max of delta(s).
    """
    pivot_table = np.round(
        dataframe.pivot_table(
            index=["Action"],
            values=["Delta(s)"],
            aggfunc={"count", "mean", "min", "max"},
        ),
        2,
    )
    return pivot_table


def create_plot(df: pd.DataFrame, prefix: string):
    # bins = np.linspace(start = pv["Delta(s)"].min(), stop = pv["Delta(s)"].max())
    # bins = np.arange(0, 600, 20)
    # # hist = pv.plot.hist(column="Delta(s)", bins = 20,range=[pv["Delta(s)"].min(), pv["Delta(s)"].max()],linewidth=1,edgecolor='black')
    _, (ax1, ax2) = plt.subplots(2, figsize=(20, 22))
    # ax1.hist(
    #     np.clip(df["Delta(s)"], bins[0], bins[-1]),
    #     bins=bins,
    #     linewidth=1,
    #     edgecolor="black",
    #     align="left",
    # )
    # ax1.set_title("All Deltas Binned")
    # ax1.set_xlabel("Seconds")
    # ax1.set_ylabel("Event Count")
    # xlabels = bins[1:].astype(str)
    # xlabels[-1] += "+"

    # N_labels = len(xlabels)
    # ax1.set_xticks(20 * np.arange(N_labels))
    # ax1.set_xticklabels(xlabels)
    # ax1.grid(axis="y")
    # ax1.set_axisbelow(True)
    # ax1.set_xlim([-20, 580])

    bins = [0, 1, 5, 10, 30, 60, 180, 360, 720, 1440, np.inf]
    binned_deltas = df.groupby(pd.cut(df["Delta(s)"], bins)).size()
    print(binned_deltas)
    ax1 = binned_deltas.plot(kind="bar", ax=ax1)
    ax1.set_title("All Deltas Binned")
    ax1.set_xlabel("Minutes")
    ax1.set_xticklabels(ax1.get_xticklabels(), rotation=0)
    ax1.set_ylabel("Event Count")
    ax1.set_axisbelow(True)
    ax1.grid(axis="y")

    # ax2.plot(pv["Delta(s)"])
    # server_time = df["Server Time"]
    # pv["Time"] =  pv['Server Date'].astype(str) +" "+ pv["Server Time"]
    # N_labels2 = len(server_time)
    # step_size = N_labels2 // 30
    # xlabels2 = pd.Series([])
    # for i in range(0, N_labels2, step_size):
    #     xlabels2[i] = server_date[i] +" "+ server_time[i][0:5]

    df["Time"] = df["Server Date"].astype(str) + " " + df["Server Time"]
    df.plot(
        ax=ax2,
        kind="line",
        x="Time",
        y="Delta(s)",
        title="Deltas Over Time",
        rot=0,
    )
    ax2.grid(axis="y")
    ax2.set_axisbelow(True)
    # xlabels2 = pv["Time"][::step_size]
    # # ax2.set_xticks(np.arange(0, N_labels2, step_size))
    # ax2.set_xticklabels(xlabels2,rotation = 75)
    ax2.get_yaxis().get_major_formatter().set_scientific(False)
    # fig.tight_layout()
    plt.subplots_adjust(hspace=0.314, bottom=0.17)
    plt.savefig(f"{prefix}_plots.png")
    plt.show()


# Open example logfile
def main():

    dev_mod, input_file, output_file = ask_user_input()
    file = open(str(input_file), "r")

    # Open csv file to write to and parse into it
    if dev_mod == "FJ2500":
        parse_2500_log(file, output_file)
    elif dev_mod == "502":
        parse_502_log(file, output_file)
    elif dev_mod.upper() == "TM95":
        parse_TM95_log(file, output_file)
    df = pd.read_csv(output_file)
    # Convert seconds to minutes
    df["Delta(s)"] = df["Delta(s)"] / 60.0
    # Create pivot table using dataframe
    pv = create_pivot_table(df)
    # Remove .csv from output file name in order to use rest of
    # name as prefix for pivot table file name
    file_prefix = output_file.split(".")[0]
    pv.to_excel(f"{file_prefix}_pivot.xlsx")
    # Call function to create desired plots
    create_plot(df, file_prefix)
    # Do not auto close program, wait for user to exit
    # while True:
    #     if input("Press 0 to exit:\n") == "0":
    #         exit()


if __name__ == "__main__":
    main()
